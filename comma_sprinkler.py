import sys
def after_word_comma(s):
    #to check the word in the text succeeded by the comma
    for i in s:
        if i[-1] == ',':
            word = i[:-1:]
            s = [i+',' if ((i == word) and s[-1] != i and (i[-1] != '.' or i[-1] != ',')) else i for i in s]
    p = before_word_comma(s)
    s1 = ' '.join(p)
    return s1
def before_word_comma(s):
    #to check the word in the text precceded by the comma
    l=[]
    for i in s:
        ind = s.index(i)
        if ind>0:
            prev_word = s[ind-1]
            if ((prev_word[-1] == ',') and (prev_word[-1] != '.') and s[0] != i):
                if(i[-1]=='.'):
                    i=i[:-1:]
                l.append(i)
                l.append(i+'.')
                #enumerate function is used to take both the index as well value iterably 
    indices = [index for index, value in enumerate(s) if value in l]
    for i in indices:
        prev_word=s[i-1]
        s=[(j+',') if j==prev_word and prev_word[-1]!=',' and prev_word[-1]!='.' else j for j in s]
    return s
#taking input from command lines
s = sys.argv[1]
s = s.split()
if len(s)>=2 and len(s)<=1000000:
    a = after_word_comma(s)
    p=a.split()
    print(after_word_comma(p))
else:
    print("enter the correct statement which is having atleast 2 characters")
